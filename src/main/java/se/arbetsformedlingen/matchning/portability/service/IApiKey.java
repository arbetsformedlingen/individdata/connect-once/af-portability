package se.arbetsformedlingen.matchning.portability.service;

import java.io.IOException;

public interface IApiKey {
    public boolean isApiKeyExist(String apiKey, String companyName) throws IOException;
}
