package se.arbetsformedlingen.matchning.portability.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import se.arbetsformedlingen.matchning.portability.model.Key;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ApiKeyService implements IApiKey {

    private static String apiKeyFileLocation;

    public ApiKeyService(
        @Value("${apikey.filelocation}") final String fileLocation
    ) {
        ApiKeyService.apiKeyFileLocation = fileLocation;
    }

    @Override
    public boolean isApiKeyExist(String apiKey, String companyName) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        // ArrayList<Key> keys = new ArrayList<>();
        try {
            Key[] keys =  mapper.readValue(new File(ApiKeyService.apiKeyFileLocation), Key[].class);
            for(Key key: keys) {
                if ((key.getApiKey().equals(apiKey)) && (key.getCompanyName().equalsIgnoreCase(companyName))) {
                    return true;
                }
            }
            return false;
        } catch (JsonMappingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
}
