package se.arbetsformedlingen.matchning.portability.model;

import java.util.Objects;

public class Key {
    private String apiKey;
    private String email;
    private String companyName;

    public Key() {
    }

    public Key(String apiKey, String email, String companyName) {
        this.apiKey = apiKey;
        this.email = email;
        this.companyName = companyName;
    }

    public String getApiKey() {
        return this.apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompanyName() {
        return this.companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Key apiKey(String apiKey) {
        setApiKey(apiKey);
        return this;
    }

    public Key email(String email) {
        setEmail(email);
        return this;
    }

    public Key companyName(String companyName) {
        setCompanyName(companyName);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Key)) {
            return false;
        }
        Key key = (Key) o;
        return Objects.equals(apiKey, key.apiKey) && Objects.equals(email, key.email) && Objects.equals(companyName, key.companyName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(apiKey, email, companyName);
    }

    @Override
    public String toString() {
        return "{" +
            " apiKey='" + getApiKey() + "'" +
            ", email='" + getEmail() + "'" +
            ", companyName='" + getCompanyName() + "'" +
            "}";
    }

}
