package se.arbetsformedlingen.matchning.portability.builder;


import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import se.arbetsformedlingen.matchning.portability.dto.*;
import se.arbetsformedlingen.matchning.taxonomy.repository.*;

import java.util.ArrayList;
import java.util.List;

public class EmployerHistoryTypeBuilder {
    private EmployerHistoryType employerHistoryType = new EmployerHistoryType();

    public EmployerHistoryTypeBuilder setId(IdentifierType id) {
        employerHistoryType.setId(id);
        return this;
    }

    public EmployerHistoryTypeBuilder setStart(String start) {
        employerHistoryType.setStart(start);
        return this;
    }

    public EmployerHistoryTypeBuilder setEnd(String end) {
        employerHistoryType.setEnd(end);
        return this;
    }

    public EmployerHistoryTypeBuilder setCurrent(Boolean current) {
        employerHistoryType.setCurrent(current);
        return this;
    }

    public EmployerHistoryTypeBuilder setAttachmentReferences(List<AttachmentReferenceType> attachmentReferences) {
        employerHistoryType.setAttachmentReferences(attachmentReferences);
        return this;
    }

    public EmployerHistoryTypeBuilder setDescriptions(List<String> descriptions) {
        employerHistoryType.getDescriptions().addAll(descriptions);
        return this;
    }

    public EmployerHistoryTypeBuilder setOrganization(OrganizationType organization) {
        employerHistoryType.setOrganization(organization);
        return this;
    }

    public EmployerHistoryTypeBuilder setPositionHistories(List<PositionHistoryType> positionHistories) {
        employerHistoryType.getPositionHistories().addAll(positionHistories);
        return this;
    }

    public EmployerHistoryType build() {
        return employerHistoryType;
    }


    public EmployerHistoryTypeBuilder withCodes(String yrkesbenamning) {
        List<PositionHistoryType> list = new ArrayList<>();
        List<JobtechTaxonomyType> jobtechTaxonomyId = new ArrayList<>();

        JobtechTaxonomyTypeBuilder jobtechTaxonomyTypeBuilder = new JobtechTaxonomyTypeBuilder();
        TaxonomyOccupationName taxonomyOccupationName = newTaxonomyOccupationName(yrkesbenamning, TaxonomyRequestTypes.occupationName);


        jobtechTaxonomyId.add(jobtechTaxonomyTypeBuilder.setDeprecatedLegacyId(yrkesbenamning)
                .setDefinition(taxonomyOccupationName.getDefinition()).setId(taxonomyOccupationName.getId())
                .setPreferredLabel(taxonomyOccupationName.getPreferredLabel())
                .setType(TaxonomyRequestTypes.occupationName.getTaxonomyType())
                .build());

        list.add(new PositionHistoryTypeBuilder().setJobtechTaxomyId(jobtechTaxonomyId).build());

        return this;
    }

    public TaxonomyOccupationName newTaxonomyOccupationName(String oldTaxonomyID, TaxonomyRequestTypes type) {

        final String baseUrl = "https://taxonomy.api.jobtechdev.se/v1/taxonomy";
        final String suffixUrl = "/legacy/convert-matching-component-id-to-new-id?matching-component-id=";
        WebClient client = WebClient.create(baseUrl);
        String newTaxonomyURL = suffixUrl + oldTaxonomyID + "&type=" + type.getTaxonomyType();
        TaxonomyOccupationName res = client
                .get()
                .uri(newTaxonomyURL)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .retrieve()
                .bodyToMono(TaxonomyOccupationName.class)
                .block();
        return res;
    }


}