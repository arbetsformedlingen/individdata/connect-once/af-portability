package se.arbetsformedlingen.matchning.taxonomy.repository;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TaxonomySkillPojo {

    @JsonProperty("taxonomy/id")
    private String id;
    @JsonProperty("taxonomy/type")
    private String type ;
    @JsonProperty("taxonomy/preferred-label")
    private String  preferredLabel;
    @JsonProperty("taxonomy/definition")
    private String definition;
    @JsonProperty("taxonomy/deprecated-legacy-id")
    private String deprecatedLegacyId;


    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getPreferredLabel() {
        return preferredLabel;
    }

    public String getDefinition() {
        return definition;
    }


    public String getDeprecatedLegacyId() {
        return deprecatedLegacyId;
    }


}
