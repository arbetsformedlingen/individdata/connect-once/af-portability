package se.arbetsformedlingen.matchning.taxonomy.repository;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class JobtechTaxonomyType {
    private String id;

    private String type ;

    private String  preferredLabel;

    private String definition;

    private String deprecatedLegacyId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPreferredLabel() {
        return preferredLabel;
    }

    public void setPreferredLabel(String preferredLabel) {
        this.preferredLabel = preferredLabel;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public String getDeprecatedLegacyId() {
        return deprecatedLegacyId;
    }

    public void setDeprecatedLegacyId(String deprecatedLegacyId) {
        this.deprecatedLegacyId = deprecatedLegacyId;
    }


}
