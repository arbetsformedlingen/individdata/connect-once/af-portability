package se.arbetsformedlingen.matchning.taxonomy.repository;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import se.arbetsformedlingen.matchning.portability.builder.EntityTypeBuilder;
import se.arbetsformedlingen.matchning.portability.builder.IdentifierTypeBuilder;
import se.arbetsformedlingen.matchning.portability.builder.PositionHistoryTypeBuilder;
import se.arbetsformedlingen.matchning.portability.dto.PositionHistoryType;
import se.arbetsformedlingen.matchning.taxonomy.model.Concept;

import se.arbetsformedlingen.matchning.portability.dto.*;
import java.util.ArrayList;
import java.util.List;

public class JobtechTaxonomyTypeBuilder {
    private JobtechTaxonomyType taxonomyType = new JobtechTaxonomyType();

    public JobtechTaxonomyTypeBuilder  setId(String id) {
        taxonomyType.setId(id);
        return this;
    }

    public JobtechTaxonomyTypeBuilder  setType(String type) {
        taxonomyType.setType( type);
        return this;
    }
    public JobtechTaxonomyTypeBuilder     setPreferredLabel(String preferredLabel) {
        taxonomyType.setPreferredLabel( preferredLabel );
        return this;
    }

    public JobtechTaxonomyTypeBuilder   setDeprecatedLegacyId(String deprecatedLegacyId) {
         taxonomyType.setDeprecatedLegacyId( deprecatedLegacyId);
        return this;
    }


    public JobtechTaxonomyTypeBuilder   setDefinition(String definition) {
        taxonomyType.setDefinition( definition);
        return this;
    }


    public JobtechTaxonomyType build() {
        return taxonomyType;
    }

    public JobtechTaxonomyTypeBuilder withCodes(String yrkesbenamning) {
        List<PositionHistoryType> list = new ArrayList<>();
        List<EntityType> joblevels = new ArrayList<>();
        List<EntityType> jobCategories = new ArrayList<>();
        IdentifierTypeBuilder identifierTypeBuilder = new IdentifierTypeBuilder();
        PositionHistoryTypeBuilder positionHistoryTypeBuilder = new PositionHistoryTypeBuilder();


        List<JobtechTaxonomyType> jobtechTaxonomyId = new ArrayList<>();
        TaxonomyOccupationName taxonomyOccupationName = newTaxonomyOccupationName(yrkesbenamning, TaxonomyRequestTypes.occupationName);
        JobtechTaxonomyTypeBuilder jobtechTaxonomyTypeBuilder = new JobtechTaxonomyTypeBuilder();


        jobtechTaxonomyId.add(jobtechTaxonomyTypeBuilder.setDeprecatedLegacyId(yrkesbenamning).setDefinition(taxonomyOccupationName.getDefinition()).setId(taxonomyOccupationName.getId()).setPreferredLabel(taxonomyOccupationName.getPreferredLabel()).setType(TaxonomyRequestTypes.occupationName.getTaxonomyType()).build());
        joblevels.add(new EntityTypeBuilder().setCode(yrkesbenamning).setName(String.valueOf(Concept.EntityType.jobterm)).build());
        list.add(new PositionHistoryTypeBuilder().setJobtechTaxomyId(jobtechTaxonomyId).setJobLevels(joblevels).setId(identifierTypeBuilder.setValue(yrkesbenamning).build()).build());

        return this;
    }
    public TaxonomyOccupationName newTaxonomyOccupationName(String oldTaxonomyID, TaxonomyRequestTypes type) {

        String baseUrl = "https://taxonomy.api.jobtechdev.se/v1/taxonomy";
        String suffixUrl = "/legacy/convert-matching-component-id-to-new-id?matching-component-id=";
        WebClient client = WebClient.create(baseUrl);
        String newTaxonomyURL = suffixUrl + oldTaxonomyID + "&type=" + type.getTaxonomyType();
        TaxonomyOccupationName res = client
                .get()
                .uri(newTaxonomyURL)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .retrieve()
                .bodyToMono(TaxonomyOccupationName.class)
                .block();
        return res;
    }

}
