package se.arbetsformedlingen.matchning.taxonomy.repository;

public enum TaxonomyRequestTypes {
    //    TODO create enums with noArgs and getters
    employmentDuration("employment-duration"),
    country("country"),
    continent("continent"),
    occupationField("occupation-field"),
    drivingLicence("driving-licence"),
    workTimeExtend("worktime-extent"),
    wageType("wage-type"),
    region("region"),
    skillHeadline("skill-headline"),
    employmentType("employment-type"),
    occupationCollection("occupation-collection"),
    occupationName("occupation-name"),
    languageLevel("language-level"),
    skill("skill"),
    sniLevel1("sni-level-1 "),
    municipality("municipality"),
    ssykLevel4("ssyk-level-4 "),
    language("language"),
    sniLevel2("sni-level-2");


    private final String taxonomyType;

    TaxonomyRequestTypes(String taxonomyType) {
        this.taxonomyType = taxonomyType;
    }

    public String getTaxonomyType() {
        return taxonomyType;

    }
}
