FROM maven:3.8.5-openjdk-11 as stage1

ARG SPRING_IDP_URL=http://af-connect-mock:9998/jwt/rest/idp/v0/klientID \
    SPRING_PROFILE_URL=http://af-connect.local:9998/arbetssokandeprofil/rest/af/v1/arbetssokandeprofil/arbetssokandeprofiler \
    SPRING_KUNDGIFT_URL=http://af-connect.local:9998/arbetssokande/rest/af/v1/arbetssokande/externa-personuppgifter \
    SPRING_DATASOURCE_URL=jdbc:postgresql://localhost:5432/apimanager \
    SPRING_OUTBOX_HOST=127.0.0.1 \
    APIKEY_FILELOCATION


ENV spring.idp.url=$SPRING_IDP_URL \
    spring.profile.url=$SPRING_PROFILE_URL \
    spring.kundgift.url=$SPRING_KUNDGIFT_URL \
    spring.datasource.url=$SPRING_DATASOURCE_URL \
    spring.outbox.host=$SPRING_OUTBOX_HOST \
    apikey.filelocation=$APIKEY_FILELOCATION \
    MAVEN_OPTS="-XX:+TieredCompilation -XX:TieredStopAtLevel=1"


RUN apt-get update &&\
    apt-get install wget
WORKDIR /build

# add the src
COPY pom.xml pom.xml
COPY src/ src
COPY libs/ libs

# maven is sensitive to man in the middle attacks
RUN mvn clean install -Dmaven.test.skip=true

#Stage 2
# set base image for second stage
FROM eclipse-temurin:11.0.21_9-jre-jammy
RUN adduser --system --group --no-create-home appuser
WORKDIR /build
COPY tmp/ ./portability-tmp

RUN chgrp -R 0 /build &&\ 
    chmod -R g=u /build &&\
    chmod -R a+rw /build
# RUN chgrp -R 0 /tmp && chmod -R g=u /tmp
# RUN chmod -R 777 /tmp


EXPOSE 8080 9804
USER appuser
COPY --from=stage1 /build/target/profile2hropen-*.jar /build/
CMD java -Dse.jobtechdev.tmp=./portability-tmp -jar ./profile2hropen-*.jar
